import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

reportsDir = "target/"

System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com/")
System.setProperty("autoqa.remoteUrl", "http://10.8.32.198:4444/wd/hub")

hubUrl = new URL(System.getProperty("autoqa.remoteUrl"))

driver = {
    def options = new ChromeOptions()
    options.addArguments("start-maximized")
    return new ChromeDriver(options)
}

environments{
    'remote-chrome'{
        driver = {
            def options = new ChromeOptions()
            options.addArguments("start-maximized")
            def capabilities = DesiredCapabilities.chrome()
            capabilities.setCapability(ChromeOptions.CAPABILITY, options)
            def driver = new RemoteWebDriver(hubUrl, capabilities)
            return driver
        }
    }
}