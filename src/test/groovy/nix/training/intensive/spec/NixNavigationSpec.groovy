package nix.training.intensive.spec

import geb.spock.GebReportingSpec
import nix.training.intensive.page.*

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"(){
        when:
            to StartPage
        and:
            "Click on Blog menu button"()
        then:
            at BlogPage
    }

}
