package nix.training.intensive.page

import geb.Page

class StartPage extends Page {
    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    static content = {
        blogLink(wait: true) { $('a', text: "Blog") }
    }

    def "Click on Blog menu button"(){
        blogLink.click()
    }
}
